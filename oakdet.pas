unit OAKDet;

interface

function DetectOAKVGA : Integer;

implementation

uses
  Graph;

function OAKVGACheck : Boolean;
var
  a, b, c : Byte;
begin
  Port[$3DE] := $0D;
  a := Port[$3DF];
  Port[$3DE] := $0D;
  Port[$3DF] := a and not $38;
  Port[$3DE] := $0D;
  b := Port[$3DF] and $38;
  Port[$3DE] := $0D;
  Port[$3DF] := a or $38;
  Port[$3DE] := $0D;
  c := Port[$3DF] and $38;
  Port[$3DE] := $0D;
  Port[$3DF] := a;
  OAKVGACheck := (b = 0) and (c = $38);
end;

function GetOAKMem : Integer;
var
  a : Byte;
begin
  Port[$3DE] := $0D;
  a := Port[$3DF];
  if (a and $40) <> 0 then
    GetOAKMem := 2
  else
    if (a and $80) <> 0 then
      GetOAKMem := 1
    else
      GetOAKMem := 0;
end;

function DetectOAKVGA : Integer;
var
  driver, mode : Integer;
begin
  if OAKVGACheck then
    case GetOAKMem of
      0: DetectOAKVGA := 0;
      1: DetectOAKVGA := 2;
      2: DetectOAKVGA := 3;
      else
         DetectOAKVGA := grError;
    end
  else
  begin
    DetectGraph(driver, mode);
    if (driver = VGA) or (driver = MCGA) then
      DetectOAKVGA := 0
    else
      DetectOAKVGA := grError;
  end;
end;

end.
