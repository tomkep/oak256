#include <graphics.h>
#include <dos.h>

static int near OAKVGACheck(void)
{
  unsigned char a, b, c;
  outportb(0x3DE, 0x0D);
  a = inportb(0x3DF);
  outportb(0x3DE, 0x0D);
  outportb(0x3DF, a & ~0x38);
  outportb(0x3DE, 0x0D);
  b = inportb(0x3DF) & 0x38;
  outportb(0x3DE, 0x0D);
  outportb(0x3DF, a | 0x38);
  outportb(0x3DE, 0x0D);
  c = inportb(0x3DF) & 0x38;
  outportb(0x3DE, 0x0D);
  outportb(0x3DF, a);
  return (b == 0 && c == 0x38);
}

static int near GetOAKMem(void)
{
  unsigned char a;
  outportb(0x3DE, 0x0D);
  a = inportb(0x3DF);
  return (a & 0x40 ? 2 : a & 0x80 ? 1 : 0);
}

int far DetectOAKVGA(void)
{
  int driver, mode;
  if(OAKVGACheck())
    switch(GetOAKMem())
    {
      case 0:  return (0);
      case 1:  return (2);
      case 2:  return (3);
      default: return (grError);
    }
  else
  {
    detectgraph(&driver, &mode);
    if((driver == VGA) || (driver == MCGA))
      return (0);
    else
      return(grError);
  }
}
