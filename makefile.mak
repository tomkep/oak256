TASM = tasm
TLINK = tlink

TLINKOPT = -x -c -t -i

!if $d(BGI30)

TASMOPT = -m -ml -jP8086 -dBGI30

!else

TASMOPT = -m -ml -jP8086

!endif

################################################################################
# implicit rules                                                               #
################################################################################

.asm.obj:
	$(TASM) $(TASMOPT) $.

################################################################################
# some shortcuts for targets                                                   #
################################################################################

oak: oak256.bgi

all: oak clean

clean:
	del *.obj
	del *.bin
	del *.bak
	del *.map

bgidemo: bgidemo.exe

################################################################################
# OAK256.BGI dependencies                                                      #
################################################################################

oak256.bgi: segs.obj oak256.obj
	$(TLINK) $(TLINKOPT) segs.obj oak256.obj, $.

oak256.obj: oak256.asm device.inc consts.inc makefile.mak

segs.obj: segs.asm makefile.mak
	$(TASM) $(TASMOPT) -DDriverName='OAK256' $&

################################################################################
# OAK VGA 256 colors demo                                                      #
################################################################################

bgidemo.exe: bgidemo.c oakdet.c makefile.mak
	bcc -O1 bgidemo.c oakdet.c graphics.lib
	del bgidemo.obj
	del oakdet.obj
