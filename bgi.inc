; This module requires TASM and IDEAL mode.

; Put modes
COPY_PUT                =       0
XOR_PUT                 =       1
OR_PUT                  =       2
AND_PUT                 =       3
NOT_PUT                 =       4
XNOR_PUT                =       5
NOR_PUT                 =       6
NAND_PUT                =       7

; Text directions
HORIZ_DIR               =       0
VERT_DIR                =       1
HORIZ_DOWN_DIR          =       2
VERT_DOWN_DIR           =       3

; Graphics errors
grOk                    =       0
grNoInitGraph           =       -1
grNotDetected           =       -2
grFileNotFound          =       -3
grInvalidDriver         =       -4
grNoLoadMem             =       -5
grNoScanMem             =       -6
grNoFloodMem            =       -7
grFontNotFound          =       -8
grNoFontMem             =       -9
grInvalidMode           =       -10
grError                 =       -11
grIOerror               =       -12
grInvalidFont           =       -13
grInvalidFontNum        =       -14
grInvalidDeviceNum      =       -15
grInvalidVersion        =       -18

; POINT structure
STRUC   POINT
        X               dw      ?
        Y               dw      ?
ENDS

; RECT structure
STRUC   RECT
        X1              dw      ?
        Y1              dw      ?
        X2              dw      ?
        Y2              dw      ?
ENDS

; STATUS structure
STRUC   STATUS
        status          db      ?
        devtype         db      ?
        xres            dw      ?
        yres            dw      ?
        xefres          dw      ?
        yefres          dw      ?
        xinch           dw      ?
        yinch           dw      ?
        aspec           dw      ?
        chsizx          db      ?
        chsizy          db      ?
        fcolors         db      ?
        bcolors         db      ?
ENDS

; Line styles
SolidLine               =       0FFFFh
DottedLine              =       0CCCCh
CenterLine              =       0FC78h
DashedLine              =       0F8F8h

; FillStyles
NoFill                  EQU     000h, 000h, 000h, 000h, 000h, 000h, 000h, 000h
SolidFill               EQU     0FFh, 0FFh, 0FFh, 0FFh, 0FFh, 0FFh, 0FFh, 0FFh
LineFill                EQU     0FFh, 0FFh, 000h, 000h, 0FFh, 0FFh, 000h, 000h
LtSlashFill             EQU     001h, 002h, 004h, 008h, 010h, 020h, 040h, 080h
SlashFill               EQU     0E0h, 0C1h, 083h, 007h, 00Eh, 01Ch, 038h, 070h
BackslashFill           EQU     0E0h, 070h, 038h, 01Ch, 00Eh, 007h, 083h, 0C1h
LtBackslashFill         EQU     080h, 040h, 020h, 010h, 008h, 004h, 002h, 001h
HatchFill               EQU     0FFh, 088h, 088h, 088h, 0FFh, 088h, 088h, 088h
XHatchFill              EQU     081h, 042h, 024h, 018h, 018h, 024h, 042h, 081h
InterleaveFill          EQU     0CCh, 033h, 0CCh, 033h, 0CCh, 033h, 0CCh, 033h
WideDotFill             EQU     080h, 000h, 008h, 000h, 080h, 000h, 008h, 000h
CloseDotFill            EQU     088h, 000h, 022h, 000h, 088h, 000h, 022h, 000h

; Default palette
DefaultPalette          EQU     010h, 000h, 001h, 002h, 003h, 004h, 005h, 014h, 007h, 038h, 039h, 03Ah, 03Bh, 03Ch, 03Dh, 03Eh, 03Fh

; Macro that makes entry code for driver
MACRO   BGI_ENTRY       DriverName
        PROC            DriverName      FAR
                        push    ds
                        push    cs
                        pop     ds
                        call    [Vector+si]
                        pop     ds
                        ret
                        db      7 DUP (0)
        EMULATE:        db      5 DUP (0)
        RESERVED:       ret
        ENDP
ENDM
